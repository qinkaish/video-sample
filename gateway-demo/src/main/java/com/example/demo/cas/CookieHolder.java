package com.example.demo.cas;

/**
 * @author kaiqin
 */
public interface CookieHolder {

    /**
     * 获取属性
     *
     * @param userKey 用户key
     * @param key     值
     * @return 结果
     */
    Object getAttr(String userKey, String key);

    /**
     * 设置值
     *
     * @param userKey userkey
     * @param key     key
     * @param attr    attr
     */
    void setAttr(String userKey, String key, Object attr);


}
