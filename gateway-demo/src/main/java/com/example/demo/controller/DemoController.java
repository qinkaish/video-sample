package com.example.demo.controller;

import org.jasig.cas.client.validation.Assertion;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author kaiqin
 */
@RestController
public class DemoController {

    @GetMapping("/a")
    public String testA() {
        System.out.println("a");
        return "a";
    }

    @GetMapping("/user/login")
    public String testUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session != null && session.getAttribute("_const_cas_assertion_") != null) {
            Assertion assertion = (Assertion) session.getAttribute("_const_cas_assertion_");
            Map<String, Object> attributes = assertion.getPrincipal().getAttributes();
            System.out.println(assertion);
            System.out.println(attributes);
        }
        return "a";
    }
}
