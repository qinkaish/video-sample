package com.example.demo.redis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author kaiqin
 */
@Configuration
public class RedisConfig {

    private static final String DEFAULT_EMPTY_STR = "-1";

    @Value("${spring.redis.host:-1}")
    private String host;

    @Value("${spring.redis.port}")
    private Integer port;

    @Value("${spring.redis.cluster.nodes:-1}")
    private String clusterNodes;

    @Value("${spring.redis.database:1}")
    private Integer database;

    @Value("${spring.redis.password}")
    private String password;

    @Bean
    public RedisConnectionFactory redisConnectionFactory() {
        if (Objects.equals(host, DEFAULT_EMPTY_STR)) {
            //集群
            RedisClusterConfiguration clusterConfiguration = new RedisClusterConfiguration();

            List<RedisNode> nodes = new ArrayList<>();
            String[] nodeAddress = Arrays.stream(clusterNodes.split(",")).toArray(String[]::new);
            for (String node : nodeAddress) {
                String[] split = node.split(":");
                nodes.add(new RedisNode(split[0], Integer.parseInt(split[1])));
            }
            clusterConfiguration.setClusterNodes(nodes);
            clusterConfiguration.setPassword(password);
            return new LettuceConnectionFactory(clusterConfiguration);
        } else {
            //单节点
            RedisStandaloneConfiguration singleServerConfig = new RedisStandaloneConfiguration();
            singleServerConfig.setHostName(host);
            singleServerConfig.setPort(port);
            singleServerConfig.setDatabase(database);
            singleServerConfig.setPassword(password);
            if (password != null && !"".equals(password) && !DEFAULT_EMPTY_STR.equals(password)) {
                singleServerConfig.setPassword(password);
            }
            return new LettuceConnectionFactory(singleServerConfig);
        }
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) throws UnknownHostException {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        System.out.println("准备就绪");
        return template;
    }

    @Bean
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
        stringRedisTemplate.setConnectionFactory(redisConnectionFactory);
        return stringRedisTemplate;
    }


}
