package com.example.demo.timer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author kaiqin
 */
@Slf4j
@EnableScheduling
@Configuration
public class ScheduleTask {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 每隔10s执行一次。
     */
    @Scheduled(fixedRate = 1000 * 10)
    public void runScheduleFixedRate() {
        Long aaa = Optional.ofNullable(stringRedisTemplate.getExpire("aaa")).orElse(0L);
        if (aaa != -2 && aaa != -1 && aaa < 30000L) {
            //续租30秒
            stringRedisTemplate.expire("aaa", 30, TimeUnit.SECONDS);
        }

    }


}
