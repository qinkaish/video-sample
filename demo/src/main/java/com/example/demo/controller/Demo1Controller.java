package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author kaiqin
 */
@Slf4j
@RestController
public class Demo1Controller {

    private static Integer TICKET = 30;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 加分布式锁时候，购买车票
     *
     * @return 结果
     */
    @GetMapping("/demo1")
    public String demo() {

        //加锁
        Boolean demoLocked = stringRedisTemplate.opsForValue().setIfAbsent("aaa", "11");
        while (!Objects.equals(demoLocked, Boolean.TRUE)) {
            demoLocked = stringRedisTemplate.opsForValue().setIfAbsent("aaa", "11");
            if (Objects.equals(demoLocked, Boolean.TRUE)) {
                break;
            }
        }

        //逻辑处理
        if (TICKET == 0) {
            System.out.println("车票不足，无法购买");
            Boolean delete = stringRedisTemplate.delete("aaa");
            if (Objects.equals(delete, Boolean.TRUE)) {
                log.info("releaseLock success!");
            }
            return "a";
        }

        System.out.println("时间：" + LocalDateTime.now() + ",购买一张车票，座位号为：" + TICKET + ",余票：" + (TICKET - 1));
        TICKET = TICKET - 1;

        //释放锁
        Boolean delete = stringRedisTemplate.delete("aaa");
        if (Objects.equals(delete, Boolean.TRUE)) {
            log.info("releaseLock success!");
        }
        return "a";
    }

}
