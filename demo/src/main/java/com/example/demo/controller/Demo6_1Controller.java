package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author kaiqin
 */
@Slf4j
@RestController
public class Demo6_1Controller {

    private static Integer TICKET = 30;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 加分布式锁时候，购买车票
     *
     * @return 结果
     */
    @GetMapping("/demo6")
    public String demo1() throws Exception {
        String uuid = UUID.randomUUID().toString();
        //加锁并设置超时时间


        Boolean demoLocked = stringRedisTemplate.opsForValue().setIfAbsent("aaa", uuid, 12, TimeUnit.SECONDS);
        while (!Objects.equals(demoLocked, Boolean.TRUE)) {
            demoLocked = stringRedisTemplate.opsForValue().setIfAbsent("aaa", uuid, 12, TimeUnit.SECONDS);
            if (Objects.equals(demoLocked, Boolean.TRUE)) {
                break;
            }
        }

        //逻辑处理
        try {
            if (TICKET == 0) {
                log.info(Thread.currentThread().getName() + ":车票不足，无法购买");
                return "a";
            }
            //卖票程序
            System.out.println(Thread.currentThread().getName() + ":时间:" + LocalDateTime.now() + ",购买一张车票，座位号为：" + TICKET + ",余票：" + TICKET);
            if (TICKET == 10) {
                //锁30秒
                Thread.sleep(30000);
            }

            //扣库存程序
            TICKET = TICKET - 1;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //释放锁
            String actualUuid = stringRedisTemplate.opsForValue().get("aaa");
            if (Objects.equals(actualUuid, uuid)) {
                Boolean delete = stringRedisTemplate.delete("aaa");
                if (Objects.equals(delete, Boolean.TRUE)) {
                    log.info(Thread.currentThread().getName() + ": releaseLock success!");
                }
            } else {
                System.out.println(Thread.currentThread().getName() + ":非当前线程加锁，无法解锁！");
                throw new Exception("售票失败！");
            }
        }
        return "a";
    }
}
