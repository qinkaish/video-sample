package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author kaiqin
 */
@Slf4j
@RestController
public class Demo2ErrorController {

    private static Integer TICKET = 30;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 加分布式锁时候，购买车票
     *
     * @return 结果
     */
    @GetMapping("/demo2/error")
    public String demo() {

        //加锁
        Boolean demoLocked = stringRedisTemplate.opsForValue().setIfAbsent("aaa", "11");
        while (!Objects.equals(demoLocked, Boolean.TRUE)) {
            demoLocked = stringRedisTemplate.opsForValue().setIfAbsent("aaa", "11");
            if (Objects.equals(demoLocked, Boolean.TRUE)) {
                break;
            }
        }

        //逻辑处理
        try {
            if (TICKET == 0) {
                System.out.println("车票不足，无法购买");
                return "a";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //释放锁
            Boolean delete = stringRedisTemplate.delete("aaa");
            if (Objects.equals(delete, Boolean.TRUE)) {
                log.info("releaseLock success!");
            }
        }

        System.out.println("时间：" + LocalDateTime.now() + ",购买一张车票，座位号为：" + TICKET + ",余票：" + TICKET);
        TICKET = TICKET - 1;
        return "a";
    }

}
