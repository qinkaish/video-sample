package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * @author kaiqin
 */
@RestController
public class Demo0Controller {

    /**
     * 之后这个值会从数据库取
     */
    private static Integer TICKET = 30;

    /**
     * 不加分布式锁时，正常购买车票
     *
     * @return 结果
     */
    @GetMapping("/demo0")
    public String demo() {
        if (TICKET == 0) {
            System.out.println("车票不足，无法购买");
            return "a";
        }
        TICKET = TICKET - 1;
        System.out.println("时间：" + LocalDateTime.now() + ",购买一张车票，座位号为：" + TICKET + ",余票：" + TICKET);
        return "a";
    }


}
