package com.example.demo.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @description: redis锁工具类
 * @author: Wzq
 * @create: 2020-09-09 11:34
 */
@Component
@Slf4j
public class RedisLockUtil {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    /**
     * 加锁
     *
     * @param key
     * @param value
     * @param timeout 过期时间
     */
    public boolean lock(String key, String value, Integer timeout) {
        return stringRedisTemplate.opsForValue().setIfAbsent(key, value, timeout, TimeUnit.SECONDS);
    }

    /**
     * 释放锁
     *
     * @param key
     */
    public void releaseLock(String key) {
        Boolean delete = stringRedisTemplate.delete(key);
        if (Objects.equals(delete, Boolean.TRUE)) {
            log.info("releaseLock success!");
        }
    }

}
