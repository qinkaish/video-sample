package com.bob.redissondemo.controller;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * @author kaiqin
 */
@RestController
public class DemoController {

    @Autowired
    private RedissonClient redisson;

    @GetMapping("/demo")
    public String demo() throws InterruptedException {
        //加锁，锁的时间要大于执行时间
        RLock lock = redisson.getLock("demo_key");
        lock.lock(20, TimeUnit.SECONDS);

        //业务逻辑
        System.out.println(LocalDateTime.now() + ":正在执行");
        Thread.sleep(10000);

        //释放锁
        lock.unlock();
        return "a";
    }

}
