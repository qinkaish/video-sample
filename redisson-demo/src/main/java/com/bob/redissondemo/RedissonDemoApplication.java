package com.bob.redissondemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author kaiqin
 */
@SpringBootApplication
public class RedissonDemoApplication {


    public static void main(String[] args) {
        SpringApplication.run(RedissonDemoApplication.class, args);
    }

}
