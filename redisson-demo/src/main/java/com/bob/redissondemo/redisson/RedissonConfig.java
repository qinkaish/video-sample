package com.bob.redissondemo.redisson;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Objects;

/**
 * redisson配置
 *
 * @author kaiqin
 */
@Configuration
public class RedissonConfig {

    private static final String DEFAULT_EMPTY_STR = "-1";

    @Value("${spring.redis.cluster.nodes:-1}")
    private String clusterNodes;

    @Value("${spring.redis.cluster.max-redirects:-1}")
    private int clusterMaxRedirects;

    @Value("${spring.redis.database:1}")
    private int database;

    @Value("${spring.redis.host:localhost}")
    private String host;

    @Value("${spring.redis.port:6379}")
    private String port;

    @Value("${spring.redis.password:-1}")
    private String password;

    @Bean(destroyMethod = "shutdown")
    public RedissonClient redisson() {
        Config config = new Config();
        if (!Objects.equals(DEFAULT_EMPTY_STR, clusterNodes)) {
            //存在cluster值，为集群部署
            String[] nodeAddress = Arrays.stream(clusterNodes.split(",")).toArray(String[]::new);
            ClusterServersConfig clusterServers = config.useClusterServers();
            if (Objects.equals(Integer.parseInt(DEFAULT_EMPTY_STR), clusterMaxRedirects)) {
                clusterServers.setScanInterval(2000);
            } else {
                clusterServers.setScanInterval(clusterMaxRedirects);
            }
            if (password != null && !"".equals(password) && !DEFAULT_EMPTY_STR.equals(password)) {
                clusterServers.setPassword(password);
            }
            clusterServers.addNodeAddress(nodeAddress);
        } else if (!DEFAULT_EMPTY_STR.equals(password)) {
            //单机版部署
            SingleServerConfig singleServerConfig = config.useSingleServer();
            singleServerConfig.setAddress(host + ":" + port);
            singleServerConfig.setDatabase(database);
            singleServerConfig.setPassword(password);
            if (password != null && !"".equals(password) && !DEFAULT_EMPTY_STR.equals(password)) {
                singleServerConfig.setPassword(password);
            }
        } else {
            throw new RuntimeException("无法确定redis的部署方式，请检查redis.yml配置！");
        }
        return Redisson.create(config);
    }

}
